<html>
	<head>
		<style>
			input
			{
				width:500px;
			}
			
			textarea
			{
				width:500px;
				height:300px;
			}
		</style>
	</head>
	
	<body>
		<form action="workshop.php" method="POST">
			<table>
				<tr>
					<td>Client ID</td>
					<td>:</td>
					<td><input type="text" name="CLIENTID" id="CLIENTID" /></td>
					<td>4ccc2669-6c99-4f5f-bda2-79b9c5898e93</td>
				</tr>
				<tr>
					<td>Client Secret</td>
					<td>:</td>
					<td><input type="text" name="CLIENTSECRET" id="CLIENTSECRET" /></td>
					<td>640b3e1a-664e-48b8-a0ff-33378069294b</td>
				</tr>
				<tr>
					<td>API Key</td>
					<td>:</td>
					<td><input type="text" name="APIKEY" id="APIKEY" /></td>
					<td>5336924e-5c28-48de-8901-a716bf3e647f</td>
				</tr>
				<tr>
					<td>API Secret</td>
					<td>:</td>
					<td><input type="text" name="APISECRET" id="APISECRET" /></td>
					<td>f64ef757-c0a0-4195-87a2-964147376df2</td>
				</tr>
				<tr>
					<td>Host</td>
					<td>:</td>
					<td><input type="text" name="HOSTGLOBAL" id="HOSTGLOBAL" /></td>
					<td>https://sandbox.bca.co.id</td>
				</tr>
				<tr>
					<td>Method</td>
					<td>:</td>
					<td><input type="text" name="METHODGLOBAL" id="METHODGLOBAL" /></td>
					<td>POST</td>
				</tr>
				<tr>
					<td>Path</td>
					<td>:</td>
					<td><input type="text" name="PATHGLOBAL" id="PATHGLOBAL" /></td>
					<td>/sakuku-commerce/payments</td>
				</tr>
				<tr>
					<td>Body</td>
					<td>:</td>
					<td>
					<textarea name="BODYGLOBAL" id="BODYGLOBAL"></textarea>
					</td>
					<td>{
     "MerchantID":"89000",
     "MerchantName":"Merchant One",
     "Amount":"100.22",
     "Tax":"0.0",  
     "TransactionID":"156479",  
     "CurrencyCode":"IDR",         
     "RequestDate":"2015-04-29T09:54:00.234+07:00",
     "ReferenceID":"123465798"   
}</td>
				</tr>
				<tr>
					<td colspan="4"><input type="submit" name="submit" id="submit" value="submit" /></td>
				</tr>
			</table>
		</form>
	</body>
</html>