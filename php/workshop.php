<?php

/*Metode untuk Call API (Start)*/

	function CallAPI($method, $url, $data = false, $header)
	{
		//$data_string = json_encode($data);
		
		$curl = curl_init();

		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
		
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
		
		curl_setopt($curl, CURLOPT_POST, false);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
		
		$result = curl_exec($curl);
		
		if($result == "")
		{
			$result = curl_error($curl);
		}
		curl_close($curl);
		
		return $result;
	}

/*Metode untuk Call API (End)*/	
	

/*Global Parameter (Start)*/
	
	$ClientID 		= $_POST["CLIENTID"];
	$ClientSecret 	= $_POST["CLIENTSECRET"];
	$APIKey			= $_POST["APIKEY"];
	$APISecret		= $_POST["APISECRET"];
	
	$HostGlobal		= $_POST["HOSTGLOBAL"];
	$MethodGlobal	= $_POST["METHODGLOBAL"];
	$PathGlobal		= $_POST["PATHGLOBAL"];
	$BodyGlobal		= $_POST["BODYGLOBAL"];
	$BodyGlobal 	= trim(preg_replace("/\s\s+/", " ", $BodyGlobal));
	$BodyGlobal 	= str_replace(" ", "", $BodyGlobal);
	date_default_timezone_set('Asia/Jakarta');
	$TimeGlobal		= date("Y-m-d\TH:i:s.000P");
	
	/* contoh data Sukses
	$ClientID 		= "4ccc2669-6c99-4f5f-bda2-79b9c5898e93";
	$ClientSecret 	= "640b3e1a-664e-48b8-a0ff-33378069294b";
	$APIKey			= "5336924e-5c28-48de-8901-a716bf3e647f";
	$APISecret		= "f64ef757-c0a0-4195-87a2-964147376df2";
	
	$HostGlobal		= "https://sandbox.bca.co.id";
	$MethodGlobal	= "POST";
	$PathGlobal		= "/sakuku-commerce/payments";
	$BodyGlobal		= json_encode(array("MerchantID" => "89000", "MerchantName" => "Merchant One", "Amount" => "100.22", "Tax" => "0.0", "TransactionID" => "156479", "CurrencyCode" => "IDR", "RequestDate" => "2015-04-29T09:54:00.234+07:00", "ReferenceID" => "123465798"), JSON_UNESCAPED_SLASHES);
	$BodyGlobal 	= str_replace(" ", "", $BodyGlobal);
	date_default_timezone_set('Asia/Jakarta');
	$TimeGlobal		= date("Y-m-d\TH:i:s.vP");
	*/
	
/*Global Parameter (End)*/
	
	
/*Call Oauth (Start)*/

	//Input Parameter
	$MethodOauth 	= "POST";
	$UrlOauth 		= $HostGlobal."/api/oauth/token";
	$BodyOauth		= "grant_type=client_credentials";
	$HeaderOauth	= array("Authorization : Basic ".base64_encode($ClientID.":".$ClientSecret));
	
	//Call function by Input Parameter
	$dataOauth = CallAPI($MethodOauth, $UrlOauth, $BodyOauth, $HeaderOauth) . PHP_EOL;
	
	//Print Result
	//echo $dataOauth;
	$resultDataOauth = json_decode($dataOauth, TRUE);
	//echo $resultDataOauth["access_token"];
	
/*Call Oauth (End)*/	


/*Call Signature (Start)*/

	//Input Parameter
	$MethodSignature 		= $MethodGlobal;
	$PathSignature			= $PathGlobal;
	$AccessTokenSignature	= $resultDataOauth["access_token"];
	$BodySignature			= $BodyGlobal;
	$TimeSignature			= $TimeGlobal;
	
	//Signature Formula
	$stringToSign = $MethodSignature.":".$PathSignature.":".$AccessTokenSignature.":".strtolower(hash('sha256', $BodySignature)).":".$TimeSignature;
	$Signature = hash_hmac('sha256', $stringToSign, $APISecret);
	
	//Print Result
	//echo $Signature;
	
/*Call Signature (End)*/
	
	
/*Call Service (Start)*/
	
	//Input Parameter
	$MethodService 	= $MethodSignature;
	$UrlService 	= $HostGlobal.$PathSignature;
	$BodyService	= $BodySignature;
	$HeaderService	= array("Authorization : Bearer ".$AccessTokenSignature, "X-BCA-Key : ".$APIKey, "X-BCA-Timestamp : ".$TimeSignature, "X-BCA-Signature : ".$Signature, "Origin : api.finhacks.id", "Content-Type : application/json");
	
	//Call function by Input Parameter
	$dataService = CallAPI($MethodService, $UrlService, $BodyService, $HeaderService) . PHP_EOL;
	
	//Print Result
	echo $dataService;
	
/*Call Service (End)*/	
?>