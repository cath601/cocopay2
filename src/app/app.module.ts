import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { Camera } from '@ionic-native/camera';
import { NgProgressModule } from '@ngx-progressbar/core';
import { IonicStorageModule } from '@ionic/storage';
import { AndroidPermissions } from '@ionic-native/android-permissions';
import { OcrProvider } from '../providers/ocr/ocr';
import { Http, HttpModule, Response } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';

import { MyApp } from './app.component';

/** Import file related to Login & Register */
import { WelcomePage } from '../pages/welcome/welcome';
import { RegisterPage } from '../pages/register/register';

/** Import file related to Home Page */
import { HomePage } from '../pages/home/home';
import { OptHomePage } from '../pages/opt-home/opt-home';
import { SummaryPage } from '../pages/summary/summary';
import { SummaryRemindPage } from '../pages/summary/summary-remind';
import { MySummaryDetailsPage } from '../pages/summary-details/my-summary-details';
import { TheirSummaryDetailsPage } from '../pages/summary-details/their-summary-details';
import { NotificationPage } from '../pages/notification/notification';

/** Import file related to Coco Page */
import { EventsPage } from '../pages/events/events';
import { ExpenseDetailsPage } from '../pages/expense-details/expense-details';
import { TagUserPage } from '../pages/tag-user/tag-user';

/** Import file related to Profile Page */

import { ProfilePage } from '../pages/profile/profile';
import { OptProfilePage } from '../pages/opt-profile/opt-profile';
import { EditProfilePage } from '../pages/edit-profile/edit-profile';
import { AllVouchersPage } from '../pages/all-vouchers/all-vouchers';
import { VoucherDetailsPage } from '../pages/voucher-details/voucher-details';
import { GradeDetailsPage } from '../pages/grade-details/grade-details';
import { PointDetailsPage } from '../pages/point-details/point-details';

/** Import other pages */
import { TabsPage } from '../pages/tabs/tabs';
import { SettingsPage } from '../pages/settings/settings';
import { HelpPage } from '../pages/help/help';
import { AboutPage } from '../pages/about/about';
import { LogoutPage } from '../pages/logout/logout';
import { PopoverPage } from '../pages/popover/popover';

import { CapturePage } from '../pages/capture/capture';
import { CaptureGooglePage } from '../pages/capture-google/capture-google';
import { ApiBcaProvider } from '../providers/api-bca/api-bca';
import { CallApiPage } from '../pages/call-api/call-api';

import { DataProvider } from '../providers/data';
import { ArrayFilterPipe } from '../pipes/array-filter.pipe';
import { ArrayExactFilterPipe } from '../pipes/array-filter.pipe';

@NgModule({
  declarations: [
    MyApp,
    WelcomePage,
    RegisterPage,
    HomePage,
    OptHomePage,
    //SummaryPage,
    //SummaryRemindPage,
    MySummaryDetailsPage,
    TheirSummaryDetailsPage,
    NotificationPage,
    EventsPage,
    ExpenseDetailsPage,
    TagUserPage,
    ProfilePage,
    OptProfilePage,
    EditProfilePage,
    AllVouchersPage,
    VoucherDetailsPage,
    GradeDetailsPage,
    PointDetailsPage,
    TabsPage,
    SettingsPage,
    HelpPage,
    AboutPage,
    LogoutPage,
    PopoverPage,
    CapturePage,
    CaptureGooglePage,
    CallApiPage,
    ArrayFilterPipe,
    ArrayExactFilterPipe

  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    NgProgressModule.forRoot(),
    IonicStorageModule.forRoot(),
    HttpClientModule,
    HttpModule
    //Http,
    //Response
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    WelcomePage,
    RegisterPage,
    HomePage,
    OptHomePage,
    //SummaryPage,
    //SummaryRemindPage,
    MySummaryDetailsPage,
    TheirSummaryDetailsPage,
    NotificationPage,
    EventsPage,
    ExpenseDetailsPage,
    TagUserPage,
    ProfilePage,
    OptProfilePage,
    EditProfilePage,
    AllVouchersPage,
    VoucherDetailsPage,
    GradeDetailsPage,
    PointDetailsPage,
    TabsPage,
    SettingsPage,
    HelpPage,
    AboutPage,
    LogoutPage,
    PopoverPage,
    CapturePage,
    CaptureGooglePage,
    CallApiPage  
    //ArrayFilterPipe,
    //ArrayExactFilterPipe  
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Camera,
    Storage,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    OcrProvider,
    AndroidPermissions,
    ApiBcaProvider,
    DataProvider
  ]
})
export class AppModule {}
