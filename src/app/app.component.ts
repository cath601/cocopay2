import { Component } from '@angular/core';
import { ViewChild } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { WelcomePage } from '../pages/welcome/welcome';
import { Storage } from '@ionic/storage';
import { Nav} from 'ionic-angular';
import { TabsPage } from '../pages/tabs/tabs';
import { AndroidPermissions } from '@ionic-native/android-permissions';

@Component({  
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
  
  user;
  constructor(
    public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    public storage: Storage,
    public androidPermissions: AndroidPermissions
    ) {
        this.initializeApp();
        this.storage.get('isLogged').then(logged => {
          if (logged) {
          this.storage.get('user').then(user => {
              if(user)
                  this.user = user;
          });
              this.nav.setRoot(TabsPage);
          } else {
              this.nav.setRoot(WelcomePage);
          }
      });

      this.user = {
          username: '',
          password: '',
      };

      
      /*this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.CAMERA).then(
        result => console.log('Has permission?',result.hasPermission),
        err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.CAMERA)
      );
      
      this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.CAMERA, this.androidPermissions.PERMISSION.GET_ACCOUNTS]);*/
      platform.ready().then(() => {

        androidPermissions.requestPermissions(
          [
            androidPermissions.PERMISSION.CAMERA, 
            androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE, 
            androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE
          ]
        );

   }) 
  }
  initializeApp() {
      this.platform.ready().then(() => {
          // Okay, so the platform is ready and our plugins are available.
          // Here you can do any higher level native things you might need.
          this.statusBar.styleDefault();
          this.splashScreen.hide();
      });
  }

//   oneSignalNotif(){
//       this.platform.ready().then(() =>{
//         this.statusBar.styleDefault();
//         this.splashScreen.hide();

//         /* OneSignal Code start:
//         Enable to debug issues:
//         window["plugins"].OneSignal.setLogLevel({logLevel: 4, visualLevel: 4}); */
//         var notificationOpenedCallback = function(jsonData){
//             console.log('notificationOpenedCallback: '+ JSON.stringify(jsonData));
//         };

//         window["plugins"].OneSignal
//         .startInit("8a1caf40-0e1d-4249-8fa1-1dbd8cf2c1c2")
//         .handleNotificationOpened(notificationOpenedCallback)
//         .endInit();
//       })
//   }
}