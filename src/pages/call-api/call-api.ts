import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, HttpModule } from '@angular/http';
import sha256, { Hash, HMAC } from "fast-sha256";
import * as shajs from 'sha.js';

/**
 * Generated class for the CallApiPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Injectable()
@Component({
  selector: 'page-call-api',
  templateUrl: 'call-api.html',
})
export class CallApiPage {

  result:any=[];
  data:Observable<any>;

  ClientID:string;
  ClientSecret:string;
  OAuthCredentials:string;
	APIKey:string;
	APISecret:string;
	
	HostGlobal:string;
	MethodGlobal:string;
  PathGlobal:string;
  StrforSignature:string;
  BodyStr:string;
  BodyGlobal:string;
  TimeGlobal:string;
  accessToken:string;
  strReqBody:string;
  now:moment.Moment;
 resultDataOauth:JSON;


 defineGlobalParam() {
  this.ClientID         = 'f26fee11-c7d1-4541-9316-f13e86e5bda0';
  this.ClientSecret     = '197475cb-5f62-42ce-b803-26517169ca28';
  this.OAuthCredentials = 'ZjI2ZmVlMTEtYzdkMS00NTQxLTkzMTYtZjEzZTg2ZTViZGEwOjE5NzQ3NWNiLTVmNjItNDJjZS1iODAzLTI2NTE3MTY5Y2EyOA==';
  this.APIKey			      = '11e0eeff-fc49-495b-8e86-9943caa83843';
  this.APISecret		    = '4f702ec6-4138-468a-9e6c-18f37977abac';
  
  this.HostGlobal		= 'https://api.finhacks.id';
  this.MethodGlobal	= 'POST';
  this.PathGlobal		= '/banking/corporates/transfers';
  this.StrforSignature = '{"CorporateID":"finhacks56","SourceAccountNumber":"8220001394","TransactionID":"00000001","TransactionDate":"2018-11-25","ReferenceID":"12345/PO/2016","CurrencyCode":"IDR","Amount":"100000.00","BeneficiaryAccountNumber":"8220001408","Remark1":"Payment from","Remark2":"CocoPAY"}';
  this.BodyStr      = `{
    "CorporateID" : "finhacks56",
    "SourceAccountNumber" : "8220001394",
    "TransactionID" : "00000001",
    "TransactionDate" : "2018-11-25",
    "ReferenceID" : "12345/PO/2016",
    "CurrencyCode" : "IDR",
    "Amount" : "100000.00",
    "BeneficiaryAccountNumber" : "8220001408",
    "Remark1" : "Payment from",
    "Remark2" : "CocoPAY"
  }`;
  //this.BodyGlobal = this.BodyStr.replace(/\s\s\t\n/, ' ');
  //this.BodyGlobal = this.BodyGlobal.replace(' ', '');
  //console.log(this.BodyGlobal);
  this.now = moment();
  this.TimeGlobal = moment(this.now.format('yyyy-MM-ddTHH:mm:ss.SSSTZD'), moment.ISO_8601).format();
  console.log(this.TimeGlobal);
}


  constructor(public navCtrl: NavController, public http: Http) {
    this.defineGlobalParam();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CallApiPage');
  }

  getData(){
    var url='https://api.finhacks.id';
    this.data = this.http.get(url);
    this.data.subscribe(data => {
      this.result.data;
    });
  }

/*
  postData(){
    var url='https://api.finhacks.id';
    let postData = "/banking/corporates/transfers";
    this.data = this.http.post(url, postData);
    this.data.subscribe(data => {
      console.log(data);
    });
  }*/

  createAccessToken() {

  }

  createSignature() {

  }

  callAPI(MethodService:string, UrlService:string, BodyService:boolean, HeaderService:string) {
    
	
  }

  postData() {
    this.accessToken = "" + this.getAccessToken();
    this.getheaderAccess();
    //console.log(this.accessToken);


  }

  getAccessToken() {
    let url = 'https://api.finhacks.id/api/oauth/token'; 
    var headers = new Headers();
    headers.append('Authorization', 'Basic ZjI2ZmVlMTEtYzdkMS00NTQxLTkzMTYtZjEzZTg2ZTViZGEwOjE5NzQ3NWNiLTVmNjItNDJjZS1iODAzLTI2NTE3MTY5Y2EyOA==' );
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    let options = new RequestOptions({ headers: headers });
    let data = 'grant_type=client_credentials';
    
    return new Promise((resolve,reject)=>{
      this.http.post(url, data, options).subscribe(res => {
        console.log("access token: " + res.json()['access_token']);
         resolve(res.json()['access_token']);
         
       }, (err) => {
         reject(err);
       });
   })
  }

  getSignature() {
    //return "lala";
    let tanggal = new Date();
    this.TimeGlobal = tanggal.toISOString();
    console.log("time: " + this.TimeGlobal);
    /*
    Retrieve Timestamp from HTTP Header (X-BCA-Timestamp)
    Retrieve the API Key form HTTP Header (X-BCA-Key)
    Lookup the API Secret corresponding to the received key in internal store
    Retrieve client HMAC from HTTP Header lowercase hexadecimal format (X-BCA-Signature)
    Calculate HMAC using the API Secret as the HMAC secret key
    Compare client HMAC with calculated HMAC*/

    //StringToSign = HTTPMethod+":"+RelativeUrl+":"+AccessToken+":"+Lowercase(HexEncode(SHA-256(RequestBody)))+":"+Timestamp
    let StringToSign = "POST:/banking/corporates/transfers:"+this.accessToken+":"+shajs('sha256').update({StrforSignature}).digest('hex')+":"+this.TimeGlobal;
    console.log("Stringtosign: " + StringToSign);
    //Signature = HMAC-SHA256(apiSecret, StringToSign)
    
    //let Signature = sha256.hmac(this.APISecret, StringToSign);

    return "lala";
  }

  getheaderAccess() {
    let url = 'https://api.finhacks.id/banking/corporates/transfers'; 
    let accessToken = "" + this.getAccessToken();
    
    let signatureStr = this.getSignature();

    var headers = new Headers();
    headers.append('Authorization', accessToken);
    headers.append('Content-Type', 'application/json');
    headers.append('Origin', '[yourdomain.com]');
    headers.append('X-BCA-Key', '34f3e930-a62a-4f61-bb35-b47ce88ec82f');
    headers.append('X-BCA-Timestamp', this.TimeGlobal);
    headers.append('X-BCA-Signature', signatureStr);
    let options = new RequestOptions({ headers: headers });
    //let data = 'grant_type=client_credentials';
    //return "lalala";
    return this.http.get(url, { headers: headers });
   
  }

}
