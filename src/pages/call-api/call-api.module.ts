import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CallApiPage } from './call-api';

@NgModule({
  declarations: [
    CallApiPage,
  ],
  imports: [
    IonicPageModule.forChild(CallApiPage),
  ],
})
export class CallApiPageModule {}
