import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { CallApiPage } from '../call-api/call-api';
/**
 * Generated class for the SummaryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

//@IonicPage()
@Component({
  selector: 'page-my-summary-details',
  templateUrl: 'my-summary-details.html',
})
export class MySummaryDetailsPage {
  
  itemCollection: any[];
  priceTotal: any;
  
  constructor(public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController) {
    this.itemCollection = [];
    this.priceTotal = 0;
    this.itemCollection.push( {
      name: "VANILLA LATTE VENTI", 
      amount: "49000"
    });
    this.itemCollection.push( {
      name: "SMOKED BEEF QUICHE", 
      amount: "34000"
    });
    this.itemCollection.push({
      name: "Service", 
      amount: "0"
    });
    this.itemCollection.push({
      name: "Tax", 
      amount: "0"
    });
    this.priceTotal = 0;
    this.itemCollection.forEach((item) => {
        this.priceTotal += Number(item.amount);
    })

  }
  
  ionViewDidLoad() {
  }

  goPay(){
    /* const alert = this.alertCtrl.create({
      title: 'Pay Now',
      subTitle: 'Choose your payment method',
      buttons:[
        { text: 'Sakuku'},
        { text: 'BCA Mobile'}
      ]
    });
    alert.present(); */
    this.navCtrl.push(CallApiPage);
  }
}
