import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MySummaryDetailsPage } from './my-summary-details';

@NgModule({
  declarations: [
    MySummaryDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(MySummaryDetailsPage),
  ],
})
export class SummaryDetailsPageModule {}
