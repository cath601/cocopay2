import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the SummaryDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-their-summary-details',
  templateUrl: 'their-summary-details.html',
})
export class TheirSummaryDetailsPage {

  itemCollection: any[];
  priceTotal: any;
  
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.itemCollection = [];
    this.priceTotal = 0;
    this.itemCollection.push( {
      name: "ICE ESPRESSO", 
      amount: "35000"
    });
    this.itemCollection.push({
      name: "COCOCAFE CAKE", 
      amount: "49000"
    });
    this.itemCollection.push({
      name: "Service", 
      amount: "4750"
    });
    this.itemCollection.push({
      name: "Tax", 
      amount: "9975"
    });
    this.priceTotal = 0;
    this.itemCollection.forEach((item) => {
        this.priceTotal += Number(item.amount);
    })

  }

}
