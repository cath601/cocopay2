import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TheirSummaryDetailsPage } from './their-summary-details';

@NgModule({
  declarations: [
    TheirSummaryDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(TheirSummaryDetailsPage),
  ],
})
export class TheirSummaryDetailsPageModule {}
