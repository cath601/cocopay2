//this is where tesseract ocr implemented to read the receipt
import { Component, ErrorHandler } from '@angular/core';
import { IonicPage, IonicErrorHandler, NavController, NavParams, ActionSheetController, LoadingController, Loading } from 'ionic-angular';
import { Camera, PictureSourceType } from '@ionic-native/camera';
import { NgProgress } from '@ngx-progressbar/core';
import * as Tesseract from 'tesseract.js';
import { FormGroup, FormArray } from "@angular/forms";
import { PICKER_OPT_SELECTED } from 'ionic-angular/umd/components/picker/picker-options';
import { TagUserPage } from '../tag-user/tag-user';


interface Receipt {
  id: number,
  itemName: string,
  itemPrice: number,
  people: any[]
}

interface Receipts extends Array<Receipt>{}

@Component({
  selector: 'page-capture',
  templateUrl: 'capture.html',
})
export class CapturePage implements ErrorHandler {
  //private readonly tesseract;
  selectedImage: string;
  imageText: string;
  itemCollection: any;
  //variables buat nampung hasil OCR
  ocrReceipt : Array<any> = [];
  ocrTax1: number;
  ocrTax2: number;
  ocrTotal: number;
  ocrSubtotal: number;
  receiptText;
  
  //array buat nampung total per orang
  shares: Receipt = {
    id: 1,
    itemName: 'lala',
    itemPrice: 123,
    people: ["helen", "febrina"]
  }

  constructor(public navCtrl: NavController, public navParams: NavParams, private actionSheetCtrl: ActionSheetController, private camera: Camera, public progress: NgProgress, private loading:LoadingController) {
    /*this.tesseract = Tesseract.create({
      workerPath: '../../assets/lib/worker.js',
      langPath: '../../assets/lib//lang/tesseract.js-',
      corePath: '../../assets/lib/index.js',
    });*/
  }

  slideChanged() {}

  selectSource() {
    let actionSheet = this.actionSheetCtrl.create({
      buttons: [
        {
          text: 'From Gallery',
          handler: () => {
            this.getPicture(this.camera.PictureSourceType.PHOTOLIBRARY);
          }
        }, {
          text: 'Capture Receipt using Camera',
          handler: () => {
            this.getPicture(this.camera.PictureSourceType.CAMERA);
          }
        }, {
          text: 'Cancel',
          role: 'cancel'
        }
      ]
    });
    actionSheet.present();
  }

  getPicture(sourceType: PictureSourceType) {
    this.camera.getPicture({
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      sourceType: sourceType,
      allowEdit: true,
      saveToPhotoAlbum: false,
      correctOrientation: true
    }).then(ImageData => {
      this.selectedImage = `data:image/jpeg;base64,${ImageData}`;
    });
  }

  recognizeImage() {
    let lc = this.loading.create({
      content: 'Processing...',
      spinner: 'bubbles'
    });
    lc.present();
    Tesseract.recognize(this.selectedImage).progress(message => {
      if(message.status === 'recognizing text')
        this.progress.set(message.progress);
    }).catch(err => console.log(err)
    ).then(result => {
      //hasil OCR ada di result.text
      //imageText container buat tampilin hasil OCR, nanti klo udah jalan bisa dihapus
      //character line break diganti dengan # buat split into array
      this.imageText = (result.text).replace(/(?:\r\n|\r|\n)/g, '#');
      this.itemCollection = [];
      this.itemCollection = this.imageText.split("#");
      console.log(this.itemCollection);
      //split hasil OCR per baris, proses per line item karena akan ditag per item
      this.ocrReceipt = [];
      for (var i=0; i<this.itemCollection.length; i++) {
        var name = ""+this.itemCollection[i].match(/.+?(?=\d)/);
        var price = Number(this.itemCollection[i].match(/\b(\w+)$/mgi));
        if(name != null && !name.includes("TOTAL") && !name.includes("SERVICE") && !name.includes("TAX") )
        this.ocrReceipt.push(
          {
            "name":name,
            "price":price
          }
        );
      }
      console.log(this.ocrReceipt);
      //ambil total untuk verifikasi hasil split bill (total seluruh orang)
      let regex = new RegExp('/(\btotal\b|\bgrandtotal\b|\bgrand total\b|\bjumlah\b)\s*(\d+)', 'mgi');
      this.ocrTotal = Number(regex.test((result.text)));

      //ambil subtotal untuk perhitungan pengenaan pajak 
      regex = new RegExp('/(\bsubtotal\b|\bsub total\b)\s*(\d+)','mgi');
      this.ocrSubtotal = Number(regex.test((result.text)));

      //ambil tax baris 1
      regex = new RegExp('/(\bservice\b|\bservices\b)\s*(\d+)','mgi');
      this.ocrTax1 = Number(regex.test((result.text)));

      //ambil tax baris 2
      regex = new RegExp('/(\btax\b|\bpajak\b|\bpb1\b|\btax 10%\b|\btax 10\.00%\b)\s*(\d+)','mgi');
      this.ocrTax2 = Number(regex.test((result.text)));

      //alert buat verifikasi hasil regex
      alert("Total: " + this.ocrTotal + " Subtotal: " + this.ocrSubtotal + " Tax1: " + this.ocrTax1 + " Tax2: " + this.ocrTax2);
      
      //push hasil OCR ke ionic-input biar bisa diedit & assign orang yg punya siapa aja

    }).finally(resultOrError => {
      this.progress.complete();
      lc.dismiss();
    });
  }
  
  handleError(err: any): void {
    console.log(err);
  }
  
  form: FormGroup

  getReceipt() {
    return (<FormArray>this.form.get('receiptItem')).controls;
  }

  tagFriends(index: Number) {
    this.navCtrl.push(TagUserPage);
  }
}
