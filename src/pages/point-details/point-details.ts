import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { DataProvider } from '../../providers/data';
/**
 * Generated class for the PointDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-point-details',
  templateUrl: 'point-details.html',
})
export class PointDetailsPage {

  pointDetail: any[];

  constructor(public navCtrl: NavController, public navParams: NavParams, private dataProv: DataProvider) {
    this.getPointData();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PointDetailsPage');
  }

  getPointData(){
    this.dataProv.getPointData().subscribe(data => this.pointDetail=data);
    console.log('point data')
  }
}
