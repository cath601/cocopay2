import { Component } from '@angular/core';
import { IonicPage, ViewController, NavController } from 'ionic-angular';
import { SettingsPage } from '../settings/settings';
import { AboutPage } from '../about/about';
import { Storage } from "@ionic/storage";
import { WelcomePage } from "../welcome/welcome";

@IonicPage()
@Component({
  selector: 'page-popover',
  templateUrl: 'popover.html',
  template: `
    <ion-list>
      <button ion-item (click)="goSettings()">Settings</button>
      <button ion-item (click)="goAbout()">About</button>
      <button ion-item (click)="goLogout()">Logout</button>
    </ion-list>
    `
})

export class PopoverPage {

  constructor(public viewCtrl: ViewController, public navCtrl: NavController, public storage: Storage) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PopoverPage');
  }

  close() {
    this.viewCtrl.dismiss();
  }

  goSettings() {
    this.viewCtrl.dismiss();
    this.navCtrl.push(SettingsPage);
  }

  goAbout() {
    this.viewCtrl.dismiss();
    this.navCtrl.push(AboutPage);
  }
  
  goLogout() {
    this.viewCtrl.dismiss();
    this.storage.clear();
    this.navCtrl.setRoot(WelcomePage);
  }
}
