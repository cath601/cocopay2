import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { Storage } from "@ionic/storage";
import { SettingsPage } from '../settings/settings';
import { AboutPage } from '../about/about';
import { HelpPage } from '../help/help';
import { WelcomePage } from '../welcome/welcome';

/**
 * Generated class for the OptionsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-opt-home',
  templateUrl: 'opt-home.html',
})
export class OptHomePage {

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public viewCtrl: ViewController,
    public storage: Storage
    ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OptionsPage');
  }

  close() {
    this.viewCtrl.dismiss();
  }

  goSettings() {
    this.viewCtrl.dismiss();
    this.navCtrl.push(SettingsPage);
  }

  goAbout() {
    this.viewCtrl.dismiss();
    this.navCtrl.push(AboutPage);
  }

  goHelp() {
    this.viewCtrl.dismiss();
    this.navCtrl.push(HelpPage);
  }
  
  goLogout() {
    this.viewCtrl.dismiss();
    this.storage.clear();
    this.navCtrl.setRoot(WelcomePage);
  }
}

