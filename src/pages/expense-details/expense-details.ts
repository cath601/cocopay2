import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DataProvider } from '../../providers/data';

/**
 * Generated class for the ExpenseDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-expense-details',
  templateUrl: 'expense-details.html',
})
export class ExpenseDetailsPage {

  expDetails: any[];

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private dataProv: DataProvider
    ) {
      this.getExpDetails();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ExpenseDetailsPage');
  }

  getExpDetails(){
    this.dataProv.getExpenseDetails().subscribe(data => this.expDetails = data);
  }

}
