import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, Nav, Keyboard, ToastController } from 'ionic-angular';
import { RegisterPage } from '../register/register';
import { TabsPage } from '../tabs/tabs';
import { Storage } from '@ionic/storage';


@Component({
  selector: 'page-welcome',
  templateUrl: 'welcome.html',
})
export class WelcomePage {

  Email:string;
  Password:string;
  user:any;
  rootPage = TabsPage;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private storage: Storage, public toastCtrl: ToastController) {
    
    this.navCtrl.swipeBackEnabled=false;
    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad WelcomePage');
  }
  
  
  login() {
    console.log("Email: " + this.Email);
    console.log("Password: " + this.Password);

    //insert login validation at here
    this.user = {
      username: this.Email,
      password: this.Password
    };
    
    if(1==1){//change to validation logic
      this.storage.set('isLogged',true);
      this.storage.set('user', this.user);
      //if user registered, push to home
      this.navCtrl.setRoot(TabsPage);
      console.log("test");
    }
  }

  createAccount() {
    this.navCtrl.push(RegisterPage);
  }

  presentToast(){
    let toast = this.toastCtrl.create({
      message: 'Message from CocoPAY! You owe Helen Rp 109,725. Pay Now?',
      duration: 7000,
      position: 'top'
    });
  
    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });
  
    toast.present();
  }
}
