import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GradeDetailsPage } from './grade-details';

@NgModule({
  declarations: [
    GradeDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(GradeDetailsPage),
  ],
})
export class GradeDetailsPageModule {}
