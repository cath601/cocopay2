import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Item, ItemSliding } from 'ionic-angular';
import { CapturePage } from '../capture/capture';
import { ExpenseDetailsPage } from '../expense-details/expense-details';

@IonicPage()
@Component({
  selector: 'page-events',
  templateUrl: 'events.html',
})
export class EventsPage {
  
  items: Array<{groupName: string, description: string, groupStatus: string}>;
  expDetails: any[];

  constructor(public navCtrl: NavController,public navParams: NavParams) {
    //load list of groups from database into 
    this.items = [
      {groupName: 'Pike Place Market', description: 'Infamous starbucks 1st shop', groupStatus: 'Unsetlled Transaction'},
      {groupName: 'Greenwich Park', description: 'Bagel..toast..anything good for picnic', groupStatus: 'All Square!'},
      {groupName: 'London Eye', description: 'Entrance tickets & other add-ons', groupStatus: 'All Square!'},
      {groupName: 'Ollivander\'s', description: 'Magic wand', groupStatus: 'All Square!'},
      {groupName: 'Hogsmeade', description: 'Yummy butterbeer', groupStatus: 'All Square!'},
      {groupName: 'Pitt Rivers Museum', description: 'Cab & brunch', groupStatus: 'All Square!'}
    ];
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EventsPage');
  }

  seeArchive() {
    console.log('see archived transaction');
  }

  addExpense(item, slidingItem: ItemSliding) {
    this.navCtrl.push(CapturePage);
  }

  archive(item, slidingItem: ItemSliding) {
    slidingItem.close();

    //if groupStatus == Unsetlled Transaction, item can't be archived shows alert 

    //if groupStatus == All Square!, item can be archived

  }

  edit(item, slidingItem: ItemSliding) {
    slidingItem.close();
  }

  seeDetail(item, slidingItem: ItemSliding) {
    slidingItem.close();
    this.navCtrl.push(ExpenseDetailsPage);
  }
}
