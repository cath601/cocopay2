import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CaptureGooglePage } from './capture-google';

@NgModule({
  declarations: [
    CaptureGooglePage,
  ],
  imports: [
    IonicPageModule.forChild(CaptureGooglePage),
  ],
})
export class CaptureGooglePageModule {}
