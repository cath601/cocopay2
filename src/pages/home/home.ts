import { Component } from '@angular/core';
import { NavController, NavParams, PopoverController, Platform, AlertController, LoadingController } from 'ionic-angular';
import { OptHomePage } from '../opt-home/opt-home';
import { MySummaryDetailsPage } from '../summary-details/my-summary-details';
import { TheirSummaryDetailsPage } from '../summary-details/their-summary-details';
import { NotificationPage } from '../notification/notification';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  
  //listofdebt: string = "spender";
  //isAndroid: boolean = false;

  myDebts: any[];
  theirDebts: any [];
  summary: any;
  checkedCount: 0;
  checked: boolean = false;
  
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public popoverCtrl: PopoverController,
    public platform: Platform,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController
  ) {  
      //this.isAndroid = platform.is('android');
      this.summary = "me";

      this.checkedCount = 0;
      this.myDebts = [];
      this.myDebts.push({
        url: "assets/imgs/smile.jpg",
        name: "Theresia Catherine", 
        amount: "83000"
      });
      this.myDebts.push({
        url: "assets/imgs/smile.jpg",
        name: "Febrina Rahdiyani", 
        amount: "125000"
      });
  
      this.theirDebts = [];
      this.theirDebts.push({
        checked:false,
        url: "assets/imgs/smile.jpg",
        name: "Theresia Catherine", 
        amount: "109725"
      });
      this.theirDebts.push({
        checked:false,
        url: "assets/imgs/smile.jpg",
        name: "Febrina Rahdiyani", 
        amount: "150150"
      });      

  }
  
  goOptions (myEvent){
    let popover = this.popoverCtrl.create(OptHomePage);
    popover.present({
      ev : myEvent
    });
  }

  goSeeTheirDetails(item){
    this.navCtrl.push(TheirSummaryDetailsPage);
  }

  goSeeMyDetails(item){
      this.navCtrl.push(MySummaryDetailsPage);
  }

  goNotifications(){
    this.navCtrl.push(NotificationPage);
  }

  theirSelected(item){
    item.checked = !item.checked;
    this.checkedCount += (item.checked)?1:-1;
  }

  presentLoading() {
    const loader = this.loadingCtrl.create({
      content: "Please wait...",
      duration: 1000
    });
    loader.present();
  }

  remindToPay(){
    //this.navCtrl.push(SummaryRemindPage);
    this.presentLoading();

    const alert = this.alertCtrl.create({
      title: 'Chill!',
      subTitle: 'Coco will make sure they get notified!',
      buttons: ['OK']
    });
    alert.present();
  }
}