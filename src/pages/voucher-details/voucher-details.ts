import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the VoucherDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-voucher-details',
  templateUrl: 'voucher-details.html',
})
export class VoucherDetailsPage {

  voucherDetails: any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.voucherDetails = "overview";
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad VoucherDetailsPage');
  }

}
