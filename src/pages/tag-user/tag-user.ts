import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController, Platform } from 'ionic-angular';

import { DataProvider } from '../../providers/data';
/**
 * Generated class for the TagUserPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-tag-user',
  templateUrl: 'tag-user.html',
})
export class TagUserPage {

  public contacts: Array<any> = [];
    selectedContactId;
    selectedContact;
    callback;
    contact;
    constructor(params: NavParams,
        public navCtrl: NavController,
        public alertController: AlertController,
        public loadingCtrl: LoadingController,
        public platform: Platform,
        private dataProvider: DataProvider) {
        this.callback = params.data.callback;
        this.contact = params.data.data || [];

    }
    ionViewDidLoad() {
        this.platform.ready().then(() => {
            document.addEventListener('resume', () => {
                this.getContacts();
            });
            this.getContacts();
        });
    };
    getContacts() {
        this.contacts = [];
        let loader = this.loadingCtrl.create({
            content: "Retrieving contact lists..."
        });
        loader.present();
        this.dataProvider.getDataContact().then(
            data => {
                loader.dismiss();
                if (data) {
                    for (var current in data) {
                        this.contacts.push(data[current]);
                    }
                } else {
                    console.error('Error retrieving customer data: Data object is empty');
                }
            },
            error => {
                loader.dismiss();
                console.error('Error retrieving customer data');
                console.dir(error);
            }
        );
    };

    doSelectContact(event: any): void {
        console.log(this.contacts);
        for (var i = 0; i < this.contacts.length; i++) {
            if (this.contacts[i].id == parseInt(this.selectedContactId)) {
                this.contact = this.contacts[i];
                this.callback(this.contact).then(() => {
                    this.navCtrl.pop();
                });
                break;
            }
        }
        console.log("wutt");
    };
    refreshPage() {
        this.getContacts();
    };
}



