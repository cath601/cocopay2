import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TagUserPage } from './tag-user';

@NgModule({
  declarations: [
    TagUserPage,
  ],
  imports: [
    IonicPageModule.forChild(TagUserPage),
  ],
})
export class TagUserPageModule {}
