import { Component } from '@angular/core';
import { IonicPage, NavController, PopoverController, ViewController } from 'ionic-angular';

import { OptProfilePage } from '../opt-profile/opt-profile';
import { EditProfilePage } from '../edit-profile/edit-profile';
import { AllVouchersPage } from '../all-vouchers/all-vouchers';
import { VoucherDetailsPage } from '../voucher-details/voucher-details';

import { GradeDetailsPage } from '../grade-details/grade-details';
import { PointDetailsPage } from '../point-details/point-details';
import { TagUserPage } from '../tag-user/tag-user';

/**
 * Generated class for the ProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})

export class ProfilePage {

  listofpromo: any;

  public user_data = {
    profile_img: 'assets/imgs/avatar.png',
    name: 'Helen Olivia',
    email: 'helenolivia@gmail.com',
    password: '1234'
  };

  constructor(
    public navCtrl: NavController,
    public popoverCtrl: PopoverController,
    public viewCtrl: ViewController
    ) {
      this.listofpromo="browseDeals";
  }
  
  goEditProfile(){
    this.viewCtrl.dismiss();
    this.navCtrl.push(EditProfilePage);
  }
  
  goSeeAllVouchers(){
    this.navCtrl.push(AllVouchersPage);
  }

  goSeeVoucherDetails(){
    this.navCtrl.push(VoucherDetailsPage);
  }

  goGradeDetails(){
    this.navCtrl.push(GradeDetailsPage);
  }

  goPointDetails(){
    this.navCtrl.push(PointDetailsPage);
    //this.navCtrl.push(TagUserPage);
  }

  goOptProfile (myEvent){
    let popover = this.popoverCtrl.create(OptProfilePage);
    popover.present({
      ev : myEvent
    });

  }
}
