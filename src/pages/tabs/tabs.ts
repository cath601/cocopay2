import { Component, NgModule } from '@angular/core';
import { IonicPage, NavController, PopoverController } from 'ionic-angular';
import { EventsPage } from '../events/events';
import { HomePage } from '../home/home';
import { ProfilePage } from '../profile/profile';
//import { PopoverPage } from '../popover/popover';

@IonicPage()
@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html',
})

export class TabsPage {
  
  tab1Root = HomePage;
  tab2Root = EventsPage;
  tab3Root = ProfilePage;

  constructor(public navCtrl: NavController, public popoverCtrl: PopoverController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TabsPage');
  }

/*   presentPopover(myEvent) {
    let popover = this.popoverCtrl.create(PopoverPage);
    popover.present({
      ev: myEvent
    });

    popover.onDidDismiss(popoverData => {
      console.log(popoverData);
    })
  } */
}
