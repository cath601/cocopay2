import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MoremenuPage } from './moremenu';

@NgModule({
  declarations: [
    MoremenuPage,
  ],
  imports: [
    IonicPageModule.forChild(MoremenuPage),
  ],
})
export class MoremenuPageModule {}
