import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SummaryRemindPage } from './summary-remind';

@IonicPage()
@Component({
  selector: 'page-summary',
  templateUrl: 'summary.html',
})
export class SummaryPage {
  myDebts: any[];
  theirDebts: any[];
  summary :any;
  checkedCount: any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.summary = "me";
    this.checkedCount = 0;
    this.myDebts = [];
    this.myDebts.push( {
      url: "assets/imgs/smile.jpg",
      name: "Theresia Catherine", 
      amount: "89000"
    });
    this.myDebts.push({
      url: "assets/imgs/smile.jpg",
      name: "Febrina Rahdiyani", 
      amount: "89000"
    });

    this.theirDebts = [];
    this.theirDebts.push( {
      checked:false,
      url: "assets/imgs/smile.jpg",
      name: "Theresia Catherine", 
      amount: "10000"
    });
    this.theirDebts.push({
      checked:false,
      url: "assets/imgs/smile.jpg",
      name: "Febrina Rahdiyani", 
      amount: "275000"
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SummaryPage');
  }

  theirSelected(item){
    item.checked = !item.checked;
    this.checkedCount += (item.checked)?1:-1;
  }
  
  remindToPay(){
    this.navCtrl.push(SummaryRemindPage);
  }
}
