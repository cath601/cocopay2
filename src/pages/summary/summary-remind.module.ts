import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SummaryRemindPage } from './summary-remind';

@NgModule({
  declarations: [
    SummaryRemindPage,
  ],
  imports: [
    IonicPageModule.forChild(SummaryRemindPage),
  ],
})
export class SummaryPageModule {}
