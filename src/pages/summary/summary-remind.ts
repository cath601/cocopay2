import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
/**
 * Generated class for the SummaryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-summary',
  templateUrl: 'summary-remind.html',
})
export class SummaryRemindPage {
  itemCollection: any[];
  priceTotal: any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.itemCollection = [];
    this.priceTotal = 0;
    this.itemCollection.push( {
      name: "Micin", 
      amount: "40000"
    });
    this.itemCollection.push({
      name: "Espresso", 
      amount: "49000"
    });
    this.priceTotal = 0;
    this.itemCollection.forEach((item) => {
        this.priceTotal += Number(item.amount);
    })

  }

  ionViewDidLoad() {
  }

  itemClicked(){
    
  }
}
