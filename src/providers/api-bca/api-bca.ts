import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the ApiBcaProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ApiBcaProvider {

  apiUrl = 'https://api.finhacks.id';

  constructor(public http: HttpClient) {
    console.log('Hello ApiBcaProvider Provider');
  }

}
