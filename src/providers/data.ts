import { Injectable } from '@angular/core';
import { Http, HttpModule, Response } from '@angular/http';
import 'rxjs/Rx';

@Injectable()
export class DataProvider {

    data: Array<any> = [];
    constructor(public http: Http, private httpModule: HttpModule) { }

    getDataContact() {
        return new Promise(resolve =>
            this.http.get('assets/json/contact.json')
                .map(this.extractData)
                .subscribe(data => {
                    this.data = data;
                    resolve(this.data);
                }));
    }

    getExpenseDetails(){
        return this.http.get('../../assets/json/expense-detail-1.json')
        .do(this.logResponse)
        .map(this.extractData);
    }

    getVoucherData(){
        return this.http.get('assets/json/voucher-data.json')
        .do(this.logResponse)
        .map(this.extractData);
    }

    getPointData(){
        return this.http.get('../../assets/json/point-detail.json')
        .do(this.logResponse)
        .map(this.extractData);
    }

    getGradeData(){
        return this.http.get('assets/json/grade-data.json')
        .do(this.logResponse)
        .map(this.extractData);
    }

    private logResponse(res:Response){
        console.log(res);
    }

    private extractData(res:Response){
        return res.json();
    }
}