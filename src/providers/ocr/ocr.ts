import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import Tesseract from 'tesseract.js';

@Injectable()
export class OcrProvider {
  private readonly tesseract;
  selectedImage: string;
  imageText: string;

  constructor(public http: HttpClient) {
    console.log('Hello OcrProvider');
    this.tesseract = Tesseract.create({
      workerPath: '../../assets/lib/worker.js',
      langPath: '../../assets/lib//lang/tesseract.js-',
      corePath: '../../assets/lib/index.js',
    });

  }

  public recognizeText(image) {
    const tesseractConfig = {
      // If you want to set the language explicitly:
      lang: 'eng', 
      // You can play around with half-documented options:
      tessedit_char_whitelist: ' 0123456789',
    };

    this.tesseract.recognize(image, tesseractConfig).progress(v => {
        // v.status is a textual string of what Tesseract is doing
        // v.progress is a 0 - 1 decimal representation of the progress.
        // The progress resets for each new v.status,
        // but the major event is v.status == "recognizing text".
        console.log(v.status, v.progress);
      }).catch(err => {
        console.error('OcrProvider: Failed to analyze text.', err);
      }).then(result => {
        // Result contains these elements:
        // blocks: Array
        // confidence: 0 - 100
        // html: string
        // lines: string[]
        // oem: "DEFAULT"
        // paragraphs: string[]
        // psm: "SINGLE_BLOCK"
        // symbols: Array
        // text: string
        // version: "3.04.00"
        // words: string[]
        // I chose to use a regex to find the 
        // correct format out of result.text.  
      }).finally(resultOrError => {
        //this.progress.complete();
      });
  }
}
